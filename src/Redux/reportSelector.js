import { createSelector } from "reselect";

export const getProductsSelector = (state) => {
    return state.reportPage.products;
}

export const getCurrentYearSelector = (state) => {
    return state.reportPage.currentYear;
}

export const getDataSelector = createSelector(getProductsSelector, getCurrentYearSelector, (products, currentYear) => {
    return (products.filter(product => product.year === currentYear)).map(product => ({ name: product.name, coordinate: [product.feature1, product.feature2] }))
})

export const getFeatureSelector = createSelector(getDataSelector, (data) => {
    return [{ name: "feature > 150", coordinate: data.filter(element => element.coordinate[0] > 150) },
    { name: "feature < 100", coordinate: data.filter(element => element.coordinate[0] < 100) },
    { name: "other feature", coordinate: data.filter(element => element.coordinate[0] <= 150 && element.coordinate[0] >= 100) }];
})

export const getYearsSelector = createSelector(getProductsSelector, (products) => {
    let years = [];
    products.forEach(product => {
        if (years.indexOf(product.year) === -1) {
            years.push(product.year);
        }
    });
    return years;
})

export const getProductDisplayModeSelector = (state) => {
    return state.reportPage.productDisplayMode;
}