import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from "redux-thunk";
import reportReducer from './reportReducer';

const reducers = combineReducers({
    reportPage: reportReducer,
})

const store = createStore(reducers, applyMiddleware(thunkMiddleware))

window.store = store;

export default store;