import { dataAPI } from '../api/api';

const SET_CURRENT_YEAR = "SET_CURRENT_YEAR";
const SET_PRODUCT_DISPLAY_MODE = "SET_PRODUCT_DISPLAY_MODE";
const SET_OTHER_DISPLAY_MODE = "SET_OTHER_DISPLAY_MODE";
const SET_PRODUCTS = "SET_PRODUCTS";


const initialState = {
    products: [],
    currentYear: 0,
    productDisplayMode: true,
};

const reportReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_PRODUCTS: {
            return {
                ...state,
                products: action.products,
            }
        }
        case SET_CURRENT_YEAR: {
            return {
                ...state,
                currentYear: action.year,
            }
        }
        case SET_PRODUCT_DISPLAY_MODE: {
            return {
                ...state,
                productDisplayMode: true,
            }
        }
        case SET_OTHER_DISPLAY_MODE: {
            return {
                ...state,
                productDisplayMode: false,
            }
        }
        default:
            return state;
    }
}

export const setCurrentYearActionCreator = (year) => ({ type: SET_CURRENT_YEAR, year });
const setProductsActionCreator = (products) => ({ type: SET_PRODUCTS, products });
const setProductDislplayModeActionCreator = () => ({ type: SET_PRODUCT_DISPLAY_MODE });
const setOtherDislplayModeActionCreator = () => ({ type: SET_OTHER_DISPLAY_MODE });

export const getInitialYearAndMode = () => (dispatch) => {
    let initialParams = dataAPI.getInitialParams();
    dispatch(setProductsActionCreator(initialParams.products));
    dispatch(setCurrentYearActionCreator(initialParams.initialYear));
}

export const setMode = (productDisplayMode) => (dispatch) => {
    productDisplayMode
        ? dispatch(setOtherDislplayModeActionCreator())
        : dispatch(setProductDislplayModeActionCreator());
}


export default reportReducer;