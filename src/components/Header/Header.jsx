import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
    return (
        <header>           
            <span><NavLink to="/MainPage">MainPage</NavLink></span>  
            <span><NavLink to="/Report">Report </NavLink></span>                 
        </header>
    );
}

export default Header;

