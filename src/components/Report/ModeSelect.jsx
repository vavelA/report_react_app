import React from 'react';

const ModeSelect = (props) => {

    const setMode = () => {
        props.setMode(props.productDisplayMode);
    }

    const modes = ['products', 'category'].map(e => <option key={e} onClick={setMode}>{e}</option>);
    
    return (
        <span>
            <select onChange={setMode}>{modes}</select>
        </span>
    );
}

export default ModeSelect;

