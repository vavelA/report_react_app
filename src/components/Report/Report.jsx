import React from 'react';
import Highcharts from 'highcharts';
import { HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, Legend, ScatterSeries } from 'react-jsx-highcharts';
import ModeSelect from './ModeSelect';
import YearSelect from './YearSelect';

const Report = (props) => {
  const productsMode = props.data.map(el => <ScatterSeries key={el.name} name={el.name} data={[el.coordinate]} />);
  const categoryMode = props.feature.map(element => <ScatterSeries name={element.name} data={element.coordinate.map(el => el.coordinate)} />)
  return (
    <div>
      <HighchartsChart>
        <Chart zoomType="xy" height="45%" />

        <Title>Report</Title>

        <Legend layout="vertical" align="right" >
          <Legend.Title>Legend</Legend.Title>
        </Legend>

        <XAxis>
          <XAxis.Title>Feature 1</XAxis.Title>
        </XAxis>

        <YAxis>
          <YAxis.Title>Feature 2</YAxis.Title>
          {props.productDisplayMode ? productsMode : categoryMode}

        </YAxis>
      </HighchartsChart>
      <div>
        <YearSelect years={props.years} currentYear={props.currentYear} setYear={props.setYear}/>
        <ModeSelect productDisplayMode={props.productDisplayMode} setMode={props.setMode}/>
      </div>

    </div>
  );
}
export default withHighcharts(Report, Highcharts);