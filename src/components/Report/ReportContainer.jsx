import React from 'react';
import { connect } from 'react-redux';
import Report from './Report';
import { getInitialYearAndMode, setCurrentYearActionCreator, setMode } from '../../Redux/reportReducer';
import { getCurrentYearSelector, getProductDisplayModeSelector, getDataSelector, getFeatureSelector, getYearsSelector } from '../../Redux/reportSelector';

class ReportContainer extends React.Component {
  componentDidMount() {
    this.props.getInitialYearAndMode()
  }

  componentWillUnmount() {
    if (!this.props.productDisplayMode){
      this.props.setMode()
    }
  }

  render() {
    return (
      <Report
        data={this.props.data}
        feature={this.props.feature}
        years={this.props.years}
        currentYear={this.props.currentYear}
        productDisplayMode={this.props.productDisplayMode}
        setYear={this.props.setCurrentYearActionCreator}
        setMode={this.props.setMode}
      />);
  }
}

const mapStateToProps = (state) => {
  return {
    currentYear: getCurrentYearSelector(state),
    productDisplayMode: getProductDisplayModeSelector(state),
    data: getDataSelector(state),
    feature: getFeatureSelector(state),
    years: getYearsSelector(state),
  }

}

export default connect(mapStateToProps, { getInitialYearAndMode, setCurrentYearActionCreator, setMode}) (ReportContainer)