import React from 'react';

const YearSelect = (props) => {

    const setYear = (event) => {
        props.setYear(Number(event.target.value));
    }

    const years = props.years.map(year => <option key={year} onClick={setYear} selected={year === props.currentYear}>{year}</option>)

    return (
        <span>
            <select onChange={setYear}>{years}</select>
        </span>
    );
}

export default YearSelect;
