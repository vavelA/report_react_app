import React from 'react';

const Content = () => {
  return (
    <div>
      <h1>About Developer</h1>
      <div>Name: Vavel Andrey</div>
      <div>Used tools: React, Redux</div>
      <div>programm language: C#, JS</div>
    </div>
  );
}

export default Content;