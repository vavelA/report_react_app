import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import ReportContainer from './components/Report/ReportContainer';
import Content from './components/Content/Content';
import Footer from './components/Footer/Footer';
import { Route } from 'react-router-dom';

const App = () => {
  return (
    <div className="app-wrapper">
      <div>
        <Header />
      </div>

      <div>
        <Route path='/MainPage' render={() => <Content />} />
        <Route path='/Report' render={() => <ReportContainer />} />
      </div>
      <div>
        <Footer />
      </div>

    </div>
  );
}

export default App;
