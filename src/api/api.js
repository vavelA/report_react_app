import products from '../Redux/data';

export const dataAPI = {

    getInitialParams () {
        const initialYear = Math.max.apply(null, products.map(product => product.year));
        return ({
            products,
            initialYear 
        });
    },
    
}
